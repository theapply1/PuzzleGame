
///define enum
 public enum SystemPopupType
{
    INFO,
    ALERT,
    RESTRICT
}
public enum TileType
{
    Grid,
    Octagon,
    Hexagon
}
public enum UnitType
{
    elephant,
    giraffe,
    hippo,
    monkey,
    panda,
    parrot,
    penguin,
    pig,
    rabbit,
    snake
}
public enum UnitState
{
    Idle,
    Move,
    Swap,
    SwapComplete,
    Clear
}
public enum ConnectTileDirection
{
    Up,
    UpRight,
    Right,
    DownRight,
    Down,
    DownLeft,
    Left,
    UpLeft
}
public enum PuzzleGameState
{
    GameInit,
    Idle,
    PuzzleSetting,
    Detecting
}


///define 



///define class