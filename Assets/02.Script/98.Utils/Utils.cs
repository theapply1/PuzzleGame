using System;
using UnityEngine;
using System.Collections;

public class SingletonGameObject<T> : MonoBehaviour where T  : MonoBehaviour
{
    private static T instance;
    public static T Instance {
        get 
        {
            if(instance == null)
            {
                string nameTag = "[{0}]";
                T findInstance = FindObjectOfType<T>();
                if(findInstance == null)
                {
                    GameObject goObject = new GameObject();
                    findInstance = goObject.AddComponent<T>();
                }
                instance = findInstance;
                instance.gameObject.name = string.Format(nameTag, instance.GetType().Name);

                
            }
            return instance;
        }
    }

}

public class PopupUI : SingletonGameObject<PopupUI>
{
    public virtual void Open()
    {

    }
    public virtual void Close()
    {

    }
}
