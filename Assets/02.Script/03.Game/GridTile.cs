using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AYellowpaper.SerializedCollections;
public class GridTile : TileBase
{
  
    
    public bool IsTopGrid {
        get { return isTopGrid; }
    }
  

    public void InitializeGrid(SerializedDictionary<ConnectTileDirection, TileBase> dictionary, bool topGrid,bool bottomGrid)
    {
        connectionDic = dictionary;
        isTopGrid = topGrid;
        isBottomGrid = bottomGrid;
    }
}
