using AYellowpaper.SerializedCollections;
using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.Tilemaps;
public class PuzzleGameController : SingletonGameObject<PuzzleGameController>
{
    [SerializeField] Tilemap currentTilemap;
    [SerializeField] List<Vector3> gridTilePosition;
    [SerializeField] GridTile gridTilePrefab;
    [SerializeField] PuzzleUnit puzzleUnitPrefab;
    [SerializeField] Transform puzzleUnitRoot;
    [SerializedDictionary] public SerializedDictionary<Vector3, TileBase> tileDic;
    [SerializedDictionary] public SerializedDictionary<TileBase, PuzzleUnit> tileUnitPair;
    [SerializeField] List<TileBase> bottomGrid;
    [SerializeField] List<TileBase> topGrid;
    [SerializeField] PuzzleGameState gameState;
    public PuzzleGameState PuzzleGameState { get { return gameState; } }

    [ContextMenu("SettingGame")]
    public void SetPreGameData()
    {
        gridTilePosition = new List<Vector3>();
        if (tileDic != null
            &&
            tileDic.Count > 0)
        {
            foreach (var item in tileDic)
            {
                if (item.Value == null)
                    continue;
                if (!Application.isPlaying)
                    DestroyImmediate(item.Value.gameObject);
                else
                    Destroy(item.Value.gameObject);
            }
            tileDic.Clear();
        }


        foreach (var pos in currentTilemap.cellBounds.allPositionsWithin)
        {
            Vector3Int localPlace = new Vector3Int(pos.x, pos.y, pos.z);
            Vector3 place = currentTilemap.CellToWorld(localPlace);
            Debug.Log(currentTilemap.CellToLocal(localPlace));
            if (currentTilemap.HasTile(localPlace))
            {
                gridTilePosition.Add(place);
            }
        }

        tileDic = new SerializedDictionary<Vector3, TileBase>();
        for (int i = 0; i < gridTilePosition.Count; i++)
        {
            GridTile tileBase = Instantiate(gridTilePrefab, this.transform);
            GameObject goObject = tileBase.gameObject;
            goObject.name = "TileNum" + i.ToString();
            goObject.transform.position = gridTilePosition[i];
            goObject.transform.parent = this.transform;
            tileDic.Add(gridTilePosition[i], tileBase);
        }


        topGrid = new List<TileBase>();
        bottomGrid = new List<TileBase>();
        foreach (var item in tileDic)
        {
            Vector3 pos = item.Value.gameObject.transform.position;
            List<Vector3> findPos = new List<Vector3>();
            //Up
            findPos.Add(pos + new Vector3(0, 1, 0));
            //Right
            findPos.Add(pos + new Vector3(1, 0, 0));
            //Down
            findPos.Add(pos + new Vector3(0, -1, 0));
            //Left
            findPos.Add(pos + new Vector3(-1, 0, 0));
            SerializedDictionary<ConnectTileDirection, TileBase> connectionDic = new SerializedDictionary<ConnectTileDirection, TileBase>();
            bool isTop = false;
            bool isBottom = false;

            for (int i = 0; i < findPos.Count; i++)
            {

                if (!tileDic.ContainsKey(findPos[i]))
                {
                    if (i == 0)
                    {
                        isTop = true;
                        topGrid.Add(item.Value);
                    }
                        
                    else if(i == 2)
                    {
                        isBottom = true;
                        bottomGrid.Add(item.Value);
                    }
                        
                    continue;
                }

                ConnectTileDirection targetDirection = (ConnectTileDirection)(i * 2);
                connectionDic.Add(targetDirection, tileDic[findPos[i]]);
            }
            ((GridTile)item.Value).InitializeGrid(connectionDic, isTop, isBottom);


        }
    }

    private void Start()
    {
        if (tileDic == null || tileDic.Count == 0)
        { SetPreGameData(); }
        if(bottomGrid == null || bottomGrid.Count == 0)
        {
            SetPreGameData();
        }
        StartCoroutine(InitGame());

    }
    IEnumerator InitGame()
    {
        gameState = PuzzleGameState.GameInit;
        foreach (var item in tileDic)
        {
            PuzzleUnit unit = GameObject.Instantiate(puzzleUnitPrefab, puzzleUnitRoot.transform);
            unit.Init(item.Value);
        }
        yield return new WaitForSeconds(1.0f);
        List<List<TileBase>> collectMatchingPuzzle = DetectingMatchingPuzzle();
        if(collectMatchingPuzzle != null && collectMatchingPuzzle.Count > 0)
        {
            yield return StartCoroutine(ResettingRoutine(collectMatchingPuzzle));
        }
        gameState = PuzzleGameState.Idle;

    }
    public TileBase GetTileByPos(Vector3 pos)
    {
        if (tileDic.ContainsKey(pos))
            return tileDic[pos];

        Debug.LogError("Tile Not Found : " + pos);
        return null;
    }

    public void RegistTileUnitPair(TileBase tile, PuzzleUnit unit)
    {
        if (tileUnitPair == null)
        {
            tileUnitPair = new SerializedDictionary<TileBase, PuzzleUnit>();
        }
        if (tileUnitPair.ContainsKey(tile))
        {
            tileUnitPair.Remove(tile);
        }
        Debug.Log("Regist Tile!");
        tileUnitPair.Add(tile, unit);
    }
    public void UnRegistTile(TileBase tile)
    {
        if (tileUnitPair.ContainsKey(tile))
        {
            tileUnitPair.Remove(tile);
        }
    }
    public void CommandTileUnitSwap(TileBase tile1, TileBase tile2)
    {
        gameState = PuzzleGameState.PuzzleSetting;
        tileUnitPair[tile1].Swap(tile2);
        tileUnitPair[tile2].Swap(tile1);
        StartCoroutine(WaitForSwapComplete(tileUnitPair[tile1], tileUnitPair[tile2]));
    }

    IEnumerator WaitForSwapComplete(PuzzleUnit unit1, PuzzleUnit unit2)
    {
        while (unit1.UnitState != UnitState.SwapComplete
            &&
            unit2.UnitState != UnitState.SwapComplete)
        {
            yield return null;
        }
        List<List<TileBase>> collectMatchingPuzzle = DetectingMatchingPuzzle();
        if (collectMatchingPuzzle == null || collectMatchingPuzzle.Count == 0)
        {
            unit1.MoveTile(unit1.TargetTile);
            unit2.MoveTile(unit2.TargetTile);
            while (unit1.UnitState != UnitState.Idle
                &&
                unit2.UnitState != UnitState.Idle)
            {
                yield return null;
            }
        }
        else
        {
            yield return StartCoroutine(ResettingRoutine(collectMatchingPuzzle));
        }
        gameState = PuzzleGameState.Idle;

        
    }

    IEnumerator ResettingRoutine(List<List<TileBase>> collectMatchingPuzzle)
    {
        for (int i = 0; i < collectMatchingPuzzle.Count; i++)
        {
            for (int j = 0; j < collectMatchingPuzzle[i].Count; j++)
            {

                if (tileUnitPair.ContainsKey(collectMatchingPuzzle[i][j]))
                    Destroy(tileUnitPair[collectMatchingPuzzle[i][j]].gameObject);
                UnRegistTile(collectMatchingPuzzle[i][j]);
                yield return new WaitForSeconds(0.1f);
            }
            yield return new WaitForSeconds(0.3f);
        }
        yield return StartCoroutine(ResettingPuzzle());
        List<List<TileBase>> matchAgain = DetectingMatchingPuzzle();
        if(matchAgain != null && matchAgain.Count > 0)
        {
            yield return StartCoroutine(ResettingRoutine(matchAgain));
        }
        
    }

    IEnumerator ResettingPuzzle()
    {

        List<PuzzleUnit> waitTargets = new List<PuzzleUnit>();
        for(int i = 0; i <bottomGrid.Count; i++)
        {
            List<TileBase> columns = GetColumnList(bottomGrid[i]);
            List<PuzzleUnit> remainPuzzle = collectRemainPuzzleUnit(columns);
            for(int j = 0; j< columns.Count; j++)
            {
                if(j<remainPuzzle.Count)
                {
                    remainPuzzle[j].MoveTile(columns[j]);
                    waitTargets.Add(remainPuzzle[j]);
                }
                else
                {
                    PuzzleUnit newPuzzle = GameObject.Instantiate(puzzleUnitPrefab,puzzleUnitRoot);
                    newPuzzle.Init(columns[j]);
                    newPuzzle.gameObject.transform.position = columns[columns.Count-1].UnitTarget.position + new Vector3(0,j - remainPuzzle.Count +1,0);
                    newPuzzle.MoveTile(columns[j]);
                    waitTargets.Add(newPuzzle);
                }
            }
        }
        while(true)
        {
            bool breakLoopFlag = true;
            for(int i = 0; i < waitTargets.Count; i++)
            {
                if (waitTargets[i].UnitState != UnitState.Idle)
                {
                    breakLoopFlag = false; break;
                }
            }
            yield return null;
            if(breakLoopFlag) { break; }
        }

    }
    List<TileBase> GetColumnList(TileBase bottomGrid)
    {
        List<TileBase> tiles = new List<TileBase>();
        TileBase search = bottomGrid;
        while(!search.IsTopGrid)
        {
            tiles.Add(search);
            search = search.connectionDic[ConnectTileDirection.Up];
        }
        tiles.Add(search);

        return tiles;
    }
    List<PuzzleUnit> collectRemainPuzzleUnit(List<TileBase> columns)
    {
        List<PuzzleUnit> puzzle = new List<PuzzleUnit>();
        for(int i =0; i<columns.Count; i++)
        {
            if (tileUnitPair.ContainsKey(columns[i]))
            {
                puzzle.Add(tileUnitPair[columns[i]]);
            }
        }
        return puzzle;
    }
   

    List<List<TileBase>> DetectingMatchingPuzzle()
    {
        ClearAllTileMemo();
        List<List<TileBase>> collectMatchPuzzle = new List<List<TileBase>>();
        foreach (var item in tileDic)
        {
            if (item.Value.connectionDic.Count == 0)
            {
                return null;
            }
            List<TileBase> left = new List<TileBase>();
            left = SearchPuzzleMatchedByDirection(ConnectTileDirection.Left, item.Value, left);
            List<TileBase> right = new List<TileBase>();
            right = SearchPuzzleMatchedByDirection(ConnectTileDirection.Right, item.Value, right);

            List<TileBase> up = new List<TileBase>();
            up = SearchPuzzleMatchedByDirection(ConnectTileDirection.Up, item.Value, up);
            List<TileBase> down = new List<TileBase>();
            down = SearchPuzzleMatchedByDirection(ConnectTileDirection.Down, item.Value, down);

            List<TileBase> combinedResult = combinedPuzzleUnit(item.Value, left, right, up, down);
            if(combinedResult.Count >= 3)
            {
                collectMatchPuzzle.Add(combinedResult);
            }
        }
        return collectMatchPuzzle;
    }
    List<TileBase> SearchPuzzleMatchedByDirection(ConnectTileDirection direction, TileBase srcTile, List<TileBase> searchResult)
    {

        ///메모이제이션에 등록되어있는 예외면 처리하지 않고 리턴함
        if (srcTile.puzzleNotMatchMemo.Contains(direction))
        {
            Debug.Log("Memo checking");
            return searchResult;
        }


        ///한방향의 끝에 도달 했을때 원래 있던 리스트를 반환
        if (!srcTile.connectionDic.ContainsKey(direction))
        {
            return searchResult;
        }
        TileBase compareTile = srcTile.connectionDic[direction];
        ///혹시 모를 예외처리 -> 타일 키가 딕셔너리에 없을때
        if (!tileUnitPair.ContainsKey(compareTile))
        {
            return searchResult;
        }



        PuzzleUnit srcPuzzleUnit = tileUnitPair[srcTile];
        PuzzleUnit ComparePuzzleUnit = tileUnitPair[compareTile];
        ///비교 대상과 유닛타입이 다르면 원래 있던 리스트를 반환
        ///비교하는 타일의 메모이제이션을 기록함.
        ///같으면 현재 있는 리스트에 추가함
        if (srcPuzzleUnit.UnitType == ComparePuzzleUnit.UnitType)
        {
            searchResult.Add(compareTile);
            SearchPuzzleMatchedByDirection(direction, compareTile, searchResult);
        }

        ConnectTileDirection memoDirection = ConnectTileDirection.Down;
        switch (direction)
        {
            case ConnectTileDirection.Up:
                memoDirection = ConnectTileDirection.Down;
                break;

            case ConnectTileDirection.Right:
                memoDirection = ConnectTileDirection.Left;
                break;

            case ConnectTileDirection.Down:
                memoDirection = ConnectTileDirection.Up;
                break;

            case ConnectTileDirection.Left:
                memoDirection = ConnectTileDirection.Right;
                break;

            default:
                return searchResult;
        }
        RecordMemoization(memoDirection, compareTile);
        return searchResult;

    }

    void RecordMemoization(ConnectTileDirection direction, TileBase tile)
    {
        if (tile.puzzleNotMatchMemo == null)
        {
            tile.puzzleNotMatchMemo = new List<ConnectTileDirection>();
        }
        tile.puzzleNotMatchMemo.Add(direction);
    }
    void ClearAllTileMemo()
    {
        foreach (var item in tileDic)
        {
            item.Value.puzzleNotMatchMemo = new List<ConnectTileDirection>();
        }
    }

    List<TileBase> combinedPuzzleUnit(TileBase center, List<TileBase> left, List<TileBase> right, List<TileBase> up, List<TileBase> down)
    {
        List<TileBase> combined = new List<TileBase>();

        bool verticalEnable = false;
        int leftCount = left.Count;
        int rightCount = right.Count;
        int upCount = up.Count;
        int downCount = down.Count;

        if (leftCount <= 0 && rightCount <= 0)
            verticalEnable = true;

        else if (leftCount * rightCount == 0)
        {
            if(upCount >1 || downCount >1)
            {
                verticalEnable = true;
            }
        }

        for (int i = 0; i<left.Count; i++)
        {
            combined.Add(left[i]);
        }
        combined.Add(center);
        for (int i = 0; i < right.Count; i++)
        {
            combined.Add(right[i]);
        }
        if(verticalEnable)
        {
            for (int i = 0; i < up.Count; i++)
            {
                combined.Add(up[i]);
            }
            for (int i = 0; i < down.Count; i++)
            {
                combined.Add(down[i]);
            }
        }

        return combined;
    }
}
