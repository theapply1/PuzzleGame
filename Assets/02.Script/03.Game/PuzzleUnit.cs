using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleUnit : MonoBehaviour
{
    [SerializeField] UnitType currentType;
    public UnitType UnitType { get { return currentType; } }
    
    [SerializeField] UnitState currentState;
    public UnitState UnitState { get { return currentState; } }

    
    [SerializeField] float speed = 0.01f;
    [SerializeField] TileBase targetTile;
    [SerializeField] TileBase swapTargetTile;
    public TileBase TargetTile {
        get { return targetTile; }
    }


    [SerializeField] SpriteRenderer spRenderer;
    [SerializeField] List<Sprite> puzzleSprite;
    Vector3 mouseStart;
    Vector3 mouseEnd;


    public void Init(TileBase target)
    {
        int pickRandom = UnityEngine.Random.Range(0, ((int)UnitType.snake) + 1);
        targetTile = target;
        currentState = UnitState.Idle;
        this.transform.position = targetTile.UnitTarget.position;
        currentType = (UnitType)pickRandom;
        spRenderer.sprite = puzzleSprite[pickRandom];
        RegistTile();
    }
    void RegistTile()
    {
        if (targetTile == null) return;
        PuzzleGameController.Instance.RegistTileUnitPair(targetTile, this);
    }
    void RegistSwapTile()
    {
        if (swapTargetTile == null) return;
        PuzzleGameController.Instance.RegistTileUnitPair(swapTargetTile, this);
    }
    public void Swap(TileBase swapTile)
    {
        swapTargetTile = swapTile;
        currentState = UnitState.Swap;
    }
    public void MoveTile(TileBase target)
    {
        targetTile = target;
        currentState = UnitState.Move;
        RegistTile();
    }
    private void Update()
    {
        stateReceive();
    }


    void stateReceive()
    {
        switch (currentState)
        {
            case UnitState.Idle:
                return;
            case UnitState.Move:
                if (targetTile == null)
                    return;
                else
                {
                    Vector3 currentPos = this.transform.position;
                    Vector3 diff = targetTile.UnitTarget.position - currentPos;
                    if(diff.magnitude <= (Time.deltaTime * speed))
                    {
                        this.transform.position = targetTile.UnitTarget.position;
                        currentState = UnitState.Idle;
                        RegistTile();
                        return;
                    }

                    diff = diff.normalized;
                    currentPos = currentPos + diff * Time.deltaTime * speed;
                    this.transform.position = currentPos;
                }
                break;
            case UnitState.Swap:
                if (swapTargetTile == null)
                    return;
                else
                {
                    Vector3 currentPos = this.transform.position;
                    Vector3 diff = swapTargetTile.UnitTarget.position - currentPos;
                    if (diff.magnitude <= (Time.deltaTime * speed))
                    {
                        this.transform.position = swapTargetTile.UnitTarget.position;
                        currentState = UnitState.SwapComplete;
                        RegistSwapTile();
                        return;
                    }

                    diff = diff.normalized;
                    currentPos = currentPos + diff * Time.deltaTime * speed;
                    this.transform.position = currentPos;
                }
                break;
            case UnitState.Clear:
                return;
            default:
                return;
        }
    }


    ///input

    
    private void OnMouseDown()
    {
        InitInput();
    }

    private void OnMouseUp()
    {
        CheckInput();
    }
    void InitInput()
    {
        mouseStart = Input.mousePosition;
        mouseEnd = Vector3.zero;
    }
    void CheckInput()
    {
        mouseEnd = Input.mousePosition;
        Vector3 diff = mouseEnd - mouseStart;
        if (PuzzleGameController.Instance.PuzzleGameState != PuzzleGameState.Idle)
            return;
        if (diff.magnitude <= 0.1f)
            return;
        
        diff = diff.normalized;
        Vector3 findPos = targetTile.transform.position;
        if(Mathf.Abs(diff.x) > Mathf.Abs(diff.y))
        {
            if(diff.x < 0)
            {
                Debug.Log("Move Left");
                findPos += new Vector3(-1, 0, 0);
            }
            else
            {
                Debug.Log("Move Right");
                findPos += new Vector3(1, 0, 0);
            }
        }
        else
        {
            if (diff.y < 0)
            {
                Debug.Log("Move Down");
                findPos += new Vector3(0, -1, 0);
            }
            else
            {
                Debug.Log("Move Up");
                findPos += new Vector3(0, 1, 0);
            }
        }

        TileBase tile = PuzzleGameController.Instance.GetTileByPos(findPos);
        if(tile != null)
        {
            PuzzleGameController.Instance.CommandTileUnitSwap(TargetTile, tile);
        }
             
    }
}

