using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AYellowpaper.SerializedCollections;
public class TileBase : MonoBehaviour
{
    [SerializedDictionary("ConnectionDirection", "ConnectTile")]
    public SerializedDictionary<ConnectTileDirection, TileBase> connectionDic;

    public List<ConnectTileDirection> puzzleNotMatchMemo;

    [SerializeField] Transform unitTarget;



    public Transform UnitTarget {
        get {
            return unitTarget;
        }
    }


    [SerializeField]
    protected bool isTopGrid;
    [SerializeField]
    protected bool isBottomGrid;
    public bool IsTopGrid {
        get { return isTopGrid; }
    }
    public bool IsBottonGrid {
        get { return isBottomGrid; }
    }


}
