using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : SingletonGameObject<UIManager>
{
    [SerializeField] Stack<PopupUI> activatePopupStack;





    void RegisterPopup(PopupUI popup)
    {
      
        activatePopupStack?.Push(popup);
    }
    void DeactivateLastPopup()
    {
        PopupUI popup =  activatePopupStack?.Pop();
        if(popup != null)
        {
            popup.Close();
        }
    }
}
